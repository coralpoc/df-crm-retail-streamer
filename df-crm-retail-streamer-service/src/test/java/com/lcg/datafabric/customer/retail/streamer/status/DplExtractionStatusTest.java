package com.lcg.datafabric.customer.retail.streamer.status;


import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.testcontainers.containers.GenericContainer;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.DynamoDBHandler;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.dpl.extractor.view.status.ViewStatus;
import com.lcg.datafabric.customer.retail.RawDBHelperData;
import com.lcg.datafabric.customer.retail.repository.RetailDynamoDBHandler;
import com.lcg.datafabric.customer.retail.service.BatchNumberHelper;
import com.lcg.datafabric.customer.retail.service.DynamoDBStatusService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class DplExtractionStatusTest {
	
	 @ClassRule
	 public static GenericContainer dynamoDb = new GenericContainer("dwmkerr/dynamodb:latest")
	            .withExposedPorts(8000);
	 
	 private static AmazonDynamoDB dynamoDbClient;
	 
	 private DynamoDBStatusService service;
	 
	 @Mock
	 private DataFabricLogger2 dataFabricLogger;
	 
	 @BeforeClass
	 public static void initializeLocalDynamoDB() {
	      dynamoDbClient = DynamoDBTestUtils.buildLocalDynamoDBClient(
	                						dynamoDb.getContainerIpAddress(),
	                						dynamoDb.getMappedPort(8000));
	      
	      
	 }
	 
	 @AfterClass
	 public static void shutdownLocalDynamoDB() {
	      dynamoDbClient.shutdown();
	 }
	 
	 @Before
	 public void init() {
		 try {
			 dynamoDbClient.deleteTable(RetailDynamoDBHandler.EXTRACTION_STATUS_TABLE_NAME);
		 }catch(ResourceNotFoundException re) {
			 log.info("Cannot delete because Table does not yet exist");
		 }
		 DplExtractorTableCreator.createRetailExtractionTable(dynamoDbClient);
		 DynamoDBTestUtils.waitForDynamoDbToBeReady(dynamoDbClient, 20000);
		 service = new DynamoDBStatusService( dataFabricLogger);
		 service.setAmazonDynamoDB(dynamoDbClient);
	 }
	 
	 
	 @Test
	 public void testDynamoDbStatusDuringRetailFlowRun() {
		 
		 LocalDateTime localDate = LocalDateTime.now();
		 service.creatRetailEntryForDate(localDate,localDate,BatchNumberHelper.getBatchNumber(localDate));
		 
		 Map<String,AttributeValue> itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowAvailableState(itemMap);
		 
		 service.updateRetailRunStatus(LocalDate.now(), ViewStatus.EXTRACTING);
		 
		 itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowExtractingState(itemMap);
		 
		 service.updateRetailRunStatus(LocalDate.now(), ViewStatus.EXTRACTED);
		 itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowExtractedState(itemMap);
		 
		 service.updateRetailRunStatus(LocalDate.now(), ViewStatus.SENT_TO_KAFKA);
		 itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowSentToKafkaState(itemMap);
		 
		 
	 }
	 
	 private void verifyRunFlowAvailableState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(DynamoDBStatusService.RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.AVAILABLE.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 String sourceAvailDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
		 
		 assertEquals(sourceAvailDate,itemMap.get(ExtractionStatusTableColumns.sourceAvailDate.name()).getS());
		 
		 String batchNo = RawDBHelperData.generateBatchNo();
		 
		 assertTrue(itemMap.get(ExtractionStatusTableColumns.batchNumber.name()).getS().startsWith(batchNo));
	 }
	 
	 private void verifyRunFlowExtractingState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(DynamoDBStatusService.RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.EXTRACTING.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionStartTime.name()));
		 
		 assertNull(itemMap.get(ExtractionStatusTableColumns.extractionEndTime.name()));
		 
		 assertNull(itemMap.get(DynamoDBStatusService.RETAIL_COL_STREAMINGENDTIME));
	 }
	 
	 private void verifyRunFlowExtractedState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(DynamoDBStatusService.RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.EXTRACTED.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionStartTime.name()));
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionEndTime.name()));
		 
		 assertNull(itemMap.get(DynamoDBStatusService.RETAIL_COL_STREAMINGENDTIME));
		 

	 }
	 
	 private void verifyRunFlowSentToKafkaState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(DynamoDBStatusService.RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.SENT_TO_KAFKA.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionStartTime.name()));
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionEndTime.name()));
		 
		 assertNotNull(itemMap.get(DynamoDBStatusService.RETAIL_COL_STREAMINGENDTIME));
		 

	 }
	 
	 
	 

	 
}