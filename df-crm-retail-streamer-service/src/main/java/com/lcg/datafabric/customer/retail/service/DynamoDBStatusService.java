package com.lcg.datafabric.customer.retail.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.Select;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.dpl.extractor.dto.ExtractionStatusKey;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.DynamoDBHandler;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.dpl.extractor.view.status.ViewStatus;
import com.lcg.datafabric.customer.retail.config.DynamoDBConfig;
import com.lcg.datafabric.customer.retail.repository.RetailDynamoDBHandler;
import ch.qos.logback.classic.Level;

@Service
public class DynamoDBStatusService {

	public final static String RETAIL_VIEW_NAME = "SFMC_RETAIL";
	public final static String RETAIL_COL_STREAMINGENDTIME = "streamingEndTime";
	private final static String RETAIL_TABLE_NAME = RetailDynamoDBHandler.EXTRACTION_STATUS_TABLE_NAME;
	private final static String DPL_TABLE_NAME = DynamoDBHandler.EXTRACTION_STATUS_TABLE_NAME;
	private final static String COLUMN_VALUE_TO_EXTRACT= RETAIL_VIEW_NAME;
	private final static Integer DPL_VIEWS_COUNT = new Integer(12);
	private final static LocalTime CUTOFF_TIME = LocalTime.of(11,0,0);
	
	private QueryRequest queryRequestBeforeCutoff;
	private QueryRequest queryRequestAfterCutoff;
	private GetItemRequest getItemRequest;
	
	private AmazonDynamoDB amazonDynamoDB;
	
	private RetailDynamoDBHandler dynamoDBHandler;
	
    private DataFabricLogger2 dataFabricLogger;
    
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME.withZone(ZoneOffset.UTC);
	
	@Autowired
	public DynamoDBStatusService(DataFabricLogger2 dataFabricLogger){
		this.amazonDynamoDB = DynamoDBConfig.createDynamoDBClient();
		this.dataFabricLogger = dataFabricLogger;
			
		dynamoDBHandler = new RetailDynamoDBHandler(amazonDynamoDB);
	}
	
	
	
	public void setAmazonDynamoDB(AmazonDynamoDB amazonDynamoDB) {
		this.amazonDynamoDB = amazonDynamoDB;
		dynamoDBHandler = new RetailDynamoDBHandler(amazonDynamoDB);
	}

	private void initDBQuery() {
		queryRequestBeforeCutoff = new QueryRequest(DPL_TABLE_NAME)
				   .withSelect(Select.COUNT)
				   .withKeyConditionExpression("#srcAvailDate=:srcDate")
				   .withFilterExpression("#status=:extractStatus")
				   .withExpressionAttributeNames(getAttributeNames())
				   .withExpressionAttributeValues(getAttributeValuesBeforeCutoff());
	
	queryRequestAfterCutoff = new QueryRequest(DPL_TABLE_NAME)
			   .withSelect(Select.COUNT)
			   .withKeyConditionExpression("#srcAvailDate=:srcDate")
			   .withFilterExpression("#status IN (:status1, :status2)")
			   .withExpressionAttributeNames(getAttributeNames())
			   .withExpressionAttributeValues(getAttributeValuesafterCutoff());
	
	getItemRequest = new GetItemRequest(RETAIL_TABLE_NAME,getKeyToExtract());
	}


	public boolean isDigitalOverDeadline() {
		return LocalTime.now().isAfter(CUTOFF_TIME);
	}
	
	
	private Map<String,String> getAttributeNames() {
		Map<String,String> expressionAttributeNames = new HashMap<>();
		expressionAttributeNames.put("#srcAvailDate",ExtractionStatusTableColumns.sourceAvailDate.name());
		expressionAttributeNames.put("#status",ExtractionStatusTableColumns.extractionStatus.name());
		return expressionAttributeNames;
	}
	
	private Map<String,AttributeValue> getAttributeValuesBeforeCutoff() {
		Map<String,AttributeValue> expressionAttributeValues = new HashMap<>();
	    expressionAttributeValues.put(":srcDate", new AttributeValue().withS(LocalDate.now().format(dateFormatter)));
	    expressionAttributeValues.put(":extractStatus", new AttributeValue().withS(ViewStatus.SENT_TO_KAFKA.name()));
	    return expressionAttributeValues;
	}
	
	private Map<String,AttributeValue> getAttributeValuesafterCutoff() {
		Map<String,AttributeValue> expressionAttributeValues = new HashMap<>();
	    expressionAttributeValues.put(":srcDate", new AttributeValue().withS(LocalDate.now().format(dateFormatter)));
	    expressionAttributeValues.put(":status1", new AttributeValue().withS(ViewStatus.READY_TO_JOIN.name()));
	    expressionAttributeValues.put(":status2", new AttributeValue().withS(ViewStatus.JOINING.name()));
	    return expressionAttributeValues;
	}
	
	private  Map<String,AttributeValue> getKeyToExtract(){
		Map<String,AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(ExtractionStatusTableColumns.sourceAvailDate.name(), new AttributeValue().withS(LocalDate.now().format(dateFormatter)));
		expressionAttributeValues.put(ExtractionStatusTableColumns.tableName.name(), new AttributeValue().withS(COLUMN_VALUE_TO_EXTRACT));
		return expressionAttributeValues;
	}
	
	private  Map<String,AttributeValue> getKeyToExtract(LocalDate runDate){
		Map<String,AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(ExtractionStatusTableColumns.sourceAvailDate.name(), new AttributeValue().withS(runDate.format(DateTimeFormatter.ISO_LOCAL_DATE)));
		expressionAttributeValues.put(ExtractionStatusTableColumns.tableName.name(), new AttributeValue().withS(RETAIL_VIEW_NAME));
		return expressionAttributeValues;
	}
	
	
	public void creatRetailEntryForDate(LocalDateTime runDate,LocalDateTime etlDateTime, String batchNo) {
		
		Map<String,AttributeValue> itemMap = new HashMap<String,AttributeValue>();
		
		itemMap.put(ExtractionStatusTableColumns.tableName.name(), new AttributeValue(RETAIL_VIEW_NAME));
		itemMap.put(ExtractionStatusTableColumns.sourceAvailDate.name(), new AttributeValue(runDate.format(dateFormatter)));
		itemMap.put(ExtractionStatusTableColumns.extractionStatus.name(), new AttributeValue(ViewStatus.AVAILABLE.name()));
		itemMap.put(ExtractionStatusTableColumns.sourceAvailTime.name(), new AttributeValue(etlDateTime.format(dateTimeFormatter)));
		itemMap.put(ExtractionStatusTableColumns.batchNumber.name(), new AttributeValue(batchNo));
		
		amazonDynamoDB.putItem(RETAIL_TABLE_NAME, itemMap);
		
	}
	
	public Map<String,AttributeValue> getRetailViewForDate(LocalDate runDate) {	
		

		try {
			GetItemRequest getItemRequest = new GetItemRequest(RETAIL_TABLE_NAME,getKeyToExtract(runDate));
			GetItemResult result = amazonDynamoDB.getItem(getItemRequest);
			
			if(null != result && null != result.getItem()) {
				return result.getItem();
			}
				

		}catch(AmazonServiceException ex) {
			dataFabricLogger.log(Level.ERROR, DynamoDBStatusService.class, 
        			"Error while checking the status of retail extraction process", Level.ERROR.levelStr, ex);
		}
		
		return null;
	}
	
	public void updateRetailRunStatus(LocalDate sourceDate, ViewStatus viewStatus) {
		
		ExtractionStatusKey statusKey = new ExtractionStatusKey();
		statusKey.setSourceAvailabilityDate(sourceDate);
		statusKey.setTableName(RETAIL_VIEW_NAME);
		
		dynamoDBHandler.setStatus(statusKey, viewStatus);
	}
	
	
}