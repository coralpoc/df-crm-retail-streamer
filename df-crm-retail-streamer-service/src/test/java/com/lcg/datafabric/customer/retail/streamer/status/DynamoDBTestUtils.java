package com.lcg.datafabric.customer.retail.streamer.status;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DynamoDBTestUtils {

    private DynamoDBTestUtils() {
    }

    public static AmazonDynamoDB buildLocalDynamoDBClient(String host, int port) {
        return AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(
                        new BasicAWSCredentials("ACCESS_KEY", "SECRET_KEY")
                ))
                .withClientConfiguration(new ClientConfiguration())
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
                        "http://" + host + ":" + port, "eu-west-2"
                ))
                .build();
    }
    
    public static void waitForDynamoDbToBeReady(AmazonDynamoDB localDynamoDbClient, int timeoutInMillis) {
        long startTime = System.currentTimeMillis();
        boolean dynamoDbReady = false;
        while (!dynamoDbReady) {
            if (System.currentTimeMillis() - startTime > timeoutInMillis) {
                throw new IllegalStateException("Timeout reached while waiting for DynamoDB to be ready");
            }
            try {
                localDynamoDbClient.listTables();
                dynamoDbReady = true;
                log.info("DynamoDB ready");
            } catch (SdkClientException e) {
                String message = e.getMessage();
                if (message != null &&
                        (message.contains("server failed to respond") || message.contains("Connection reset"))) {
                    log.info("DynamoDB not ready yet, waiting");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        throw new IllegalStateException(e1);
                    }
                } else {
                    throw e;
                }
            }
        }
    }

}
