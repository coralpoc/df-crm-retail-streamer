package com.lcg.datafabric.customer.retail.streamer.status;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.DynamoDBHandler;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.retail.repository.RetailDynamoDBHandler;


public class DplExtractorTableCreator {

    private DplExtractorTableCreator() {
    }

    public static void createRetailExtractionTable(AmazonDynamoDB dynamoDB) {
        dynamoDB.createTable(new CreateTableRequest()
                .withTableName(RetailDynamoDBHandler.EXTRACTION_STATUS_TABLE_NAME)
                .withKeySchema(
                        new KeySchemaElement().withKeyType(KeyType.HASH).withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name()),
                        new KeySchemaElement().withKeyType(KeyType.RANGE).withAttributeName(ExtractionStatusTableColumns.tableName.name())
                )
                .withAttributeDefinitions(
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name())
                                .withAttributeType(ScalarAttributeType.S),
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.tableName.name())
                                .withAttributeType(ScalarAttributeType.S)
                )
                .withProvisionedThroughput(new ProvisionedThroughput()
                        .withReadCapacityUnits(2L)
                        .withWriteCapacityUnits(2L)
                )
          );
    }
    
    public static void createDplExtractionTable(AmazonDynamoDB dynamoDB) {
        dynamoDB.createTable(new CreateTableRequest()
                .withTableName(DynamoDBHandler.EXTRACTION_STATUS_TABLE_NAME)
                .withKeySchema(
                        new KeySchemaElement().withKeyType(KeyType.HASH).withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name()),
                        new KeySchemaElement().withKeyType(KeyType.RANGE).withAttributeName(ExtractionStatusTableColumns.tableName.name())
                )
                .withAttributeDefinitions(
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name())
                                .withAttributeType(ScalarAttributeType.S),
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.tableName.name())
                                .withAttributeType(ScalarAttributeType.S)
                )
                .withProvisionedThroughput(new ProvisionedThroughput()
                        .withReadCapacityUnits(2L)
                        .withWriteCapacityUnits(2L)
                )
          );
    }

}
