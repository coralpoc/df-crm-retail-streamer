package com.lcg.datafabric.customer.retail.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.customer.cdm.model.CustomerRecord;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.dpl.extractor.view.status.ViewStatus;
import com.lcg.datafabric.customer.retail.RawDBHelperData;

import com.lcg.datafabric.customer.retail.service.DynamoDBStatusService;
import com.lcg.datafabric.customer.retail.service.ExtractionService;
import com.lcg.datafabric.customer.retail.service.StreamerService;
import com.lcg.datafabric.customer.retail.streamer.status.DynamoDBTestUtils;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class ExtractionServiceIT {
	
	private final static String TEST_TOPIC = "df-customer-v2";
	
	@ClassRule 
	public static KafkaEmbedded embeddedKafka = new KafkaEmbedded( 1, true, 1, TEST_TOPIC);
	
	@ClassRule
	 public static GenericContainer dynamoDb = new GenericContainer("dwmkerr/dynamodb:latest")
	            .withExposedPorts(8000);
	 
	 private static AmazonDynamoDB dynamoDbClient;
	 
	 @Autowired
	 private DynamoDBStatusService service;
	 
	 @Autowired
	 private ExtractionService extractionService;
	 

	 
	 
	 @BeforeClass
	 public static void initializeLocalDynamoDB() {
	      dynamoDbClient = DynamoDBTestUtils.buildLocalDynamoDBClient(
	                						dynamoDb.getContainerIpAddress(),
	                						dynamoDb.getMappedPort(8000));
	      DynamoDBTestUtils.waitForDynamoDbToBeReady(dynamoDbClient, 30000);
	      
	 }
	 
	 @AfterClass
	 public static void shutdownLocalDynamoDB() {
	      dynamoDbClient.shutdown();
	 }
	 
	 @TestConfiguration
	 public static class config{
		 
		 @Bean
		 public DataFabricLogger2 dataFabricLogger() {
			 return new DataFabricLogger2("Test_Retail_Service");
		 }
		 
		 @Primary
		 @Bean
		 public DynamoDBStatusService dynamoService() {
			 DynamoDBStatusService dynamoDBStatusService =  new DynamoDBStatusService(dataFabricLogger());
			 dynamoDBStatusService.setAmazonDynamoDB(dynamoDbClient);
			 return dynamoDBStatusService;
		 }
		 
		 //@Primary
		 @Bean
		 public StreamerService streamerService() {
			 return new StreamerService(embeddedKafka.getBrokersAsString());
		 }
		 
		 @Bean
		 public ExtractionService extractionService() {
			 ExtractionService extractionService = new ExtractionService();
			 extractionService.setStreamerService(streamerService());
			 extractionService.setDynamoDBStatusService(dynamoService());
			 extractionService.setDataFabricLogger(dataFabricLogger());
			 
			 
			 return extractionService;
		 }
		 
	 }
	 
	 @Test
	 /*public void testEndToEndExtractionOperation() {
		 
		 extractionService.setHiveRepository(hiveRepository);
		 when(hiveRepository.fetchAllRetailData()).thenReturn(getAllRetailData());
		 
		 ControlTableResponse controlTableResp = new ControlTableResponse();
		 controlTableResp.setEtlDateTime(LocalDateTime.now());
		 controlTableResp.setRecordCount(1000);
		 
		 when(hiveRepository.queryControlTableForAvailableData(LocalDate.now()))
		 	.thenReturn(Optional.of(controlTableResp));
		 
		 extractionService.startExtraction();
		 
		 verifyDynamoDbStatusSet();
		 
		 CustomerKafkaConsumer kafkaConsumer = new CustomerKafkaConsumer(embeddedKafka);
		 List<CustomerRecord> recordList = kafkaConsumer.pullAvailableKafkaRecords(TEST_TOPIC);
		 
		 verifyCustomerRecordIsCorrect(recordList);
	 }*/
	 
	 private void verifyCustomerRecordIsCorrect(List<CustomerRecord> recordList) {
		 
		 assertThat(is(recordList.size()).equals(3));
	 }
	 
	 private void verifyDynamoDbStatusSet() {
		 
		 Map<String, AttributeValue> itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 assertTrue(!itemMap.isEmpty());
		 
		 assertEquals(DynamoDBStatusService.RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.SENT_TO_KAFKA.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 String sourceAvailDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
		 
		 assertEquals(sourceAvailDate,itemMap.get(ExtractionStatusTableColumns.sourceAvailDate.name()).getS());
		 
		 String batchNo = RawDBHelperData.generateBatchNo();
		 
		 assertTrue(itemMap.get(ExtractionStatusTableColumns.batchNumber.name()).getS().startsWith(batchNo));
	 }
	 
	 private List<Map<String,Object>> getAllRetailData(){
		 
		 RawDBHelperData rawDBHelper = new RawDBHelperData();
		 return rawDBHelper.buildMapDataFromFile("data/raw-retail-data-correct.txt");
	 }
		 

}
