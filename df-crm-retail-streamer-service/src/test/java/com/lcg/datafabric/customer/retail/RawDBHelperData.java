package com.lcg.datafabric.customer.retail;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.lcg.datafabric.customer.retail.service.BatchNumberHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RawDBHelperData {
	
	public RawDBHelperData() {
		
	}
	
	public static String getFilePath(String fileName) {
		return RawDBHelperData.class.getClassLoader().getResource(fileName).getPath();
	}
	
	
	private List<String> getFileItemasList(String filePath){
		List<String> allLines = null;
		 try {
				allLines = Files.readAllLines( Paths.get(filePath), StandardCharsets.UTF_8);
			} catch (IOException e) {
				log.error("Cannot read file content from path - {}", filePath,e);
				fail();
			}
		 
		 return allLines;
	}
	
	public Map<String,Object> buildMapRow(String[] headers,int rowSize, String dataRow){
		
		Map<String,Object> mapObj = new HashMap<String,Object>();
		
		
		String[] data = dataRow.split(",");
		Object[] dataObjs = Arrays.asList(data).toArray();
		for(int i=0; i< rowSize; i++) {
			
			mapObj.put(headers[i], dataObjs[i]);
		}
		
		return mapObj;
	}
	
	
	public List<Map<String,Object>> buildMapDataFromFile(String fileName){
		
		List<Map<String,Object>> dataset = new ArrayList<Map<String,Object>>();
		
		fileName = getFilePath(fileName);
		
		List<String> dataList = getFileItemasList(fileName);
		
		if(dataList != null) {
			String[] headers = dataList.get(0).split(",");
			int cSize = headers.length;

			for(int i = 1; i < dataList.size(); i++) {
				
				dataset.add(buildMapRow(headers,cSize,dataList.get(i)));
			}
		}
		
		return dataset;
	}
	
	public static String generateBatchNo() {
		 return BatchNumberHelper.BATCH_PREFIX + "_" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
	 }
	
	@Test
	public void testDataExtractionFromFile() {
		
		List<Map<String,Object>> mappedData = buildMapDataFromFile("data/raw-retail-data-correct.txt");
		assertNotNull(mappedData);
		assertTrue(mappedData.size() > 0);
	}

}
