package com.lcg.datafabric.customer.retail.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.dpl.extractor.view.status.ViewStatus;

import ch.qos.logback.classic.Level;

public class ExtractionService {
	
	private LocalDate todayRun;
	
	@Value("${spring.application.name}")
	private String appName;
	
	@Autowired
	private DataFabricLogger2 dataFabricLogger;
	
	@Autowired
	private DynamoDBStatusService dynamoService;
	
	@Autowired
	private StreamerService streamerservice;
	
	@Autowired
	private ELKReportService eLKReportService;
	
	public void setStreamerService(StreamerService streamerservice) {
		this.streamerservice = streamerservice;
	}
	
	public void setDynamoDBStatusService(DynamoDBStatusService dynamoService) {
		this.dynamoService = dynamoService;
	}
	
	public void setDataFabricLogger(DataFabricLogger2 dataFabricLogger) {
		this.dataFabricLogger = dataFabricLogger;
	}
	
	public void startExtraction() {
		
		try {
		
			todayRun = LocalDate.now();
			String msg = String.format("Started extraction process at %s", LocalDateTime.now());
			dataFabricLogger.log(Level.INFO, ExtractionService.class, msg, "INFO" );
			
			msg = String.format("Checking control table for data availability for date - %s", todayRun);
			dataFabricLogger.log(Level.DEBUG, ExtractionService.class, msg, "DEBUG" );
			
			LocalDateTime runDate = LocalDateTime.now();
			String batchNo = BatchNumberHelper.getBatchNumber(runDate);
			
	
			
			if(true){
				
				//ControlTableResponse contResponse = optionResponse.get();
				
				//dataFabricLogger.log(Level.INFO, ExtractionService.class, "Found load status record in hive, for etlDate " + contResponse.etlDateTime + " with record count - " + contResponse.getRecordCount()  , "INFO" );
				
				
				
				//eLKReportService.sendExtractionPerViewInfoToELK(DynamoDBStatusService.RETAIL_VIEW_NAME, contResponse);
				
				//dynamoService.creatRetailEntryForDate(runDate, contResponse.getEtlDateTime(),batchNo);
				
				dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Created a new entry in dynamoDB for retail", "DEBUG" );
				
				
				
				dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTING);
				
				dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to EXTRACTING", "DEBUG" );
				
				//List<Map<String, Object>> dataList = hiveRepository.fetchAllRetailData();
				
				dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTED);
				
				dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to EXTRACTED", "DEBUG" );
				
				if(true) {
					
					//eLKReportService.sendExtractorInfoToELK(appName, batchNo, dataList.size(), "Extracted this many rows from BI Hive for RETAIL");
					
					//msg = String.format("Retail Data available for date - %s and record extracted count is %s ", contResponse.etlDateTime,contResponse.recordCount);
					dataFabricLogger.log(Level.INFO, ExtractionService.class, msg, "INFO");
					
					dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Started streaming extracted records to kafka", "DEBUG" );
					//streamerservice.processExtracts(batchNo,dataList);
					
					dynamoService.updateRetailRunStatus(todayRun, ViewStatus.SENT_TO_KAFKA);
					dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to SENT_TO_KAFKA", "DEBUG" );
					
					dataFabricLogger.log(Level.INFO, ExtractionService.class, "Retail extraction flow completed", "INFO");
					
				}else {
					dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTION_FAILED);
					dataFabricLogger.log(Level.INFO, ExtractionService.class, "Connection error at Hive, marked status as EXTRACTION_FAILED", "INFO");
				}
				
			}else {

				dataFabricLogger.log(Level.INFO, ExtractionService.class, "Retail data not yet available in hive", "INFO");
			}
		
		}catch(Exception e) {
			
			dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTION_FAILED);
			
			dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to EXTRACTION_FAILED", "DEBUG" );
			
			dataFabricLogger.log(Level.ERROR, ExtractionService.class, "An unexpceted error occurred during extraction flow.", "ERROR", e);
		}
	}
	
	
	private boolean isValidExtractedData(List<Map<String, Object>> dataList) {
		
		boolean flag = true;
		
		if(dataList == null) {
			flag = false;
		}else if(dataList.isEmpty()) {
			dataFabricLogger.log(Level.INFO, ExtractionService.class, "This should not happen, no record extracted from hive", "INFO");
			flag = false;
		}
		
		return flag;
	}


}
