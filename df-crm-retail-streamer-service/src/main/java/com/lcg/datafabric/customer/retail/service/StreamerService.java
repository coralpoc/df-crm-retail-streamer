package com.lcg.datafabric.customer.retail.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.customer.cdm.field.StringField;
import com.lcg.customer.cdm.model.CustomerRecord;
import com.lcg.customer.common.event.CustomerStreamerEventType;
import com.lcg.datafabric.utils.KafkaSender;
import com.lcg.transform.serialization.CustomerStreamerObjectMapperFactory;
import com.lcg.transform.service.CustomerStreamerRecordProcessor;
import com.lcg.transform.service.FlowType;

import ch.qos.logback.classic.Level;

@Component
public class StreamerService {
	
	@Value("${kafka.target-topic}")
	private String targetTopic;
	
	private KafkaSender kafkaSender;
	
	@Autowired
	private DataFabricLogger2 logger;
	
	@Autowired
	private ELKReportService eLKReportService;
	
	private CustomerStreamerRecordProcessor recordProcessor;
	
	public final static String KEY_BOOTSTRAP_SERVER = "kafka.bootstrapServer";
	
	private final ObjectMapper objectMapper;
	
	private int sentToKafkaCount;
	
	public StreamerService(@Value("${kafka.bootstrapServer}") String kafkaBootstrapServer) {
		Properties properties = getConfigProperty(kafkaBootstrapServer);
	
		
		kafkaSender = KafkaSender.getInstance(properties);
		recordProcessor = CustomerStreamerRecordProcessor.getInstance(properties, kafkaSender);
		
		objectMapper = CustomerStreamerObjectMapperFactory.createForSerialisation();

	}
	
	
	public Properties getConfigProperty(String kafkaBootstrapServer) {
		
		Properties properties = new Properties();
		properties.put(KEY_BOOTSTRAP_SERVER, kafkaBootstrapServer);
		properties.put(CustomerStreamerRecordProcessor.PROPS_IS_MARKETING_FIELD_LOGIC_REQUESTED, false);
		properties.put(CustomerStreamerRecordProcessor.PROPS_KEY_DER_ENRICHMENT_ENABLED, false);
		properties.put(CustomerStreamerRecordProcessor.PROPS_KEY_FLOW_TYPE, FlowType.RETAIL);
		
		return properties;
	}
	
	public void processExtracts(String batchNo,List<Map<String,Object>> extracts) {
		
		sentToKafkaCount = 0;
		int processingCount = 0;
		int size = extracts.size();
		
		for(Map<String,Object> recordMap: extracts) {
			processingCount++;
			logger.log(Level.INFO, StreamerService.class, "Streaming " + processingCount + " of " + size + " records...", "INFO");
			streamEachRecord(batchNo,recordMap);
			recordMap = null;
		}
		logger.log(Level.INFO, StreamerService.class, "Completed streaming all records (count - " + sentToKafkaCount + ") for BatchNo - " + batchNo, "INFO");
		eLKReportService.sendExtractorInfoToELK(ELKReportService.RECORDS_SENT_TO_KAFKA, batchNo,
				sentToKafkaCount, "Retail counts sent to kafka");
	}
	
	public void streamEachRecord(String batchNo,Map<String,Object> recordMap) {
		try {
			CustomerRecord customerRecord = recordProcessor.processRecord(recordMap);
			setFlowBatchNo(batchNo,customerRecord);
			sendMessageToKafka(customerRecord);
	
			logger.log(Level.INFO, StreamerService.class, "Completed streaming record for customerId - " + 
					customerRecord.getPunter().getAccount().getCustomerIdentifier().getValue(), "INFO");
		}catch(Exception e) {
			logger.log(Level.ERROR, StreamerService.class, "Unexpected error while streaming record - " + recordMap, "ERROR",e);
		}
	}
	
	public void setFlowBatchNo(String batchNo,CustomerRecord customerRecord) {
		customerRecord.getRecordMetadata().getBatchMetadata().setLastUpdatedByBatch(new StringField(batchNo));
	}
	
	
	public void sendMessageToKafka(CustomerRecord customerRec) {
		
        try {
            
        	Map<String, Object> wrappedRecord = wrapWithMetadata(customerRec);
        	String customerIdentifier = customerRec.getPunter().getAccount().getCustomerIdentifier().getValue();
            byte[] messageBody = objectMapper.writeValueAsBytes(wrappedRecord);
            
            String msg = String.format("Sending to Kafka customer - %s -> %s", customerIdentifier,
            		customerRec);
            
            logger.log(Level.DEBUG, StreamerService.class, msg, "DEBUG");
            
            kafkaSender.sendMessage(getTargetTopic(), customerIdentifier, messageBody);
            
            msg = String.format("Sent to Kafka customerIdentifier - %s", customerIdentifier);
            
            sentToKafkaCount++;
            
            
            logger.log(Level.DEBUG, StreamerService.class, msg, "DEBUG");
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }
	
	public String getTargetTopic() {
		return targetTopic;
	}
	
	private Map<String, Object> wrapWithMetadata(final CustomerRecord customerRecord) {
        return new HashMap<String, Object>() {{
            put("metadata", new HashMap<String, Object>() {{
                put("eventType", new HashMap<String, String>() {{
                    put("name", CustomerStreamerEventType.CUSTOMER_STREAMER_EVENT_TYPE_NAME);
                }});
                put("eventSource", new HashMap<String, String>() {{
                    put("name", "RETAIL");
                }});
            }});
            put("payload", customerRecord);
        }};
    }

}
