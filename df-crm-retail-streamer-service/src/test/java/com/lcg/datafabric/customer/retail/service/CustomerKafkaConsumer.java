package com.lcg.datafabric.customer.retail.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.Deserializer;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.lcg.common.AbstractEventSource;
import com.lcg.common.AbstractEventType;
import com.lcg.customer.cdm.model.CustomerRecord;
import com.lcg.customer.cdm.serializer.CustomerRecordModule;
import com.lcg.customer.common.serialization.CustomerEventSourceDeserializer;
import com.lcg.customer.common.serialization.CustomerEventTypeDeserializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomerKafkaConsumer {

	private CustomerDeserializer deserializer;
	private KafkaEmbedded embeddedKafka;

	public CustomerKafkaConsumer(KafkaEmbedded embeddedKafka) {

		this.deserializer = new CustomerDeserializer();
		this.embeddedKafka = embeddedKafka;
	}
	
	public List<CustomerRecord> pullAvailableKafkaRecords(String topic){
		
		List<CustomerRecord> customerRecords = new ArrayList<CustomerRecord>();
		
		KafkaConsumer kafkaConsumer = initialiseKafkaConsumer(topic);
		try{  
			  int count = 0;
			  int retry = 3;
		    while(count < retry) { 
		    	count++;
			    ConsumerRecords<byte[], byte[]> records = kafkaConsumer.poll(100);  
			    
			    log.debug("Consummed records for topic: {} count: {}",
		    			topic,records.count());
			    
			    for(ConsumerRecord<byte[], byte[]> record : records) {
			    	
			    	log.debug("Processing record, topic: {} key: {} value: {}",
			    			topic,new String(record.key()),new String(record.value()));
			    	
			    	customerRecords.add(deserializer.deserialize(topic, record.value()));    
		    	}        
		    }    
		 } finally{        
		    kafkaConsumer.close();    
		  }
		
		return customerRecords;
	}
	
	public KafkaConsumer initialiseKafkaConsumer(String topic) {
		KafkaConsumer<byte[], byte[]> kafkaConsumer = new KafkaConsumer<>(getConsumerProperties());
		  kafkaConsumer.subscribe(Collections.singletonList(topic));
		  
		  return kafkaConsumer;
	}
	
	public Map<String, Object> getConsumerProperties(){
		
		Map<String, Object> consumerProps =
				  KafkaTestUtils.consumerProps("sreamer-group", "false", embeddedKafka);
				consumerProps.put("auto.offset.reset", "earliest");  
				consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
				consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class.getName());
		
				return consumerProps;
	}

	public class CustomerDeserializer implements Deserializer<CustomerRecord> {

		ObjectMapper objectMapper = CustomerClientObjectMapperFactory.createObjectMapper();

		@Override
		public void configure(Map configs, boolean isKey) {
			// TODO Auto-generated method stub

		}

		@Override
		public CustomerRecord deserialize(String topic, byte[] data) {
			// TODO Auto-generated method stub
			CustomerRecord customerRecord = null;
			try {
				customerRecord = objectMapper.readValue(data, CustomerEvent.class).getPayload();
			} catch (Exception e) {
				String rawBodyContent = new String(data);
				log.error("Failed to deserialize message - {} {}", rawBodyContent, e);
			}

			return customerRecord;
		}

		@Override
		public void close() {
			// TODO Auto-generated method stub

		}

	}
	
	public static class CustomerClientObjectMapperFactory {

	    private CustomerClientObjectMapperFactory() {
	    }

	    public static ObjectMapper createObjectMapper() {
	        ObjectMapper objectMapper = new ObjectMapper();
	        objectMapper.registerModule(createEventModule());
	        objectMapper.registerModule(new CustomerRecordModule());
	        return objectMapper;
	    }

	    private static SimpleModule createEventModule() {
	        SimpleModule deserializerModule = new SimpleModule();
	        deserializerModule.addDeserializer(AbstractEventType.class, new CustomerEventTypeDeserializer());
	        deserializerModule.addDeserializer(AbstractEventSource.class, new CustomerEventSourceDeserializer());
	        return deserializerModule;
	    }
	}

}
