package com.lcg.datafabric.customer.retail.streamer.status;

import java.time.LocalDate;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.DynamoDBHandler;
import com.lcg.datafabric.customer.retail.repository.RetailDynamoDBHandler;

public class DplExtractorTableInsertion {
	
	private static final String DPLTableName = DynamoDBHandler.EXTRACTION_STATUS_TABLE_NAME;
	
	private DplExtractorTableInsertion() {}
	
	 public static void insertExtractionTable(AmazonDynamoDB dynamoDBClient) {
		 DynamoDB dynamoDB = new DynamoDB(dynamoDBClient);
		 TableWriteItems insertMultipleItems = new TableWriteItems(DPLTableName)
		     .withItemsToPut(
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_1").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_2").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_3").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_4").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_5").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_6").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_7").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_8").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_9").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_10").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_11").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_12").withString("extractionStatus","SENT_TO_KAFKA")
		     );
		 dynamoDB.batchWriteItem(insertMultipleItems);
	 }
	 
	 public static void simulateBatch0InProgress(AmazonDynamoDB dynamoDBClient) {
		 DynamoDB dynamoDB = new DynamoDB(dynamoDBClient);
		 TableWriteItems insertMultipleItems = new TableWriteItems(DPLTableName)
		     .withItemsToPut(
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_1").withString("extractionStatus","AVAILABLE"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_2").withString("extractionStatus","EXTRACTED"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_3").withString("extractionStatus","EXTRACTING"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_4").withString("extractionStatus","EXTRACTED"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_5").withString("extractionStatus","EXTRACTED"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_6").withString("extractionStatus","EXTRACTED")
		         );
		 dynamoDB.batchWriteItem(insertMultipleItems);
	 }
	 
	 public static void simulateBatch0Complete(AmazonDynamoDB dynamoDBClient) {
		 DynamoDB dynamoDB = new DynamoDB(dynamoDBClient);
		 TableWriteItems insertMultipleItems = new TableWriteItems(DPLTableName)
		     .withItemsToPut(
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_1").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_2").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_3").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_4").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_5").withString("extractionStatus","SENT_TO_KAFKA"),
		         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_6").withString("extractionStatus","SENT_TO_KAFKA")
		         );
		 dynamoDB.batchWriteItem(insertMultipleItems);
	 }
	 
	 public static void simulateBatchJoining(AmazonDynamoDB dynamoDBClient) {
		 DynamoDB dynamoDB = new DynamoDB(dynamoDBClient);
		 TableWriteItems insertMultipleItems = new TableWriteItems(DPLTableName)
		     .withItemsToPut(
		    		 new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_1").withString("extractionStatus","AVAILABLE"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_2").withString("extractionStatus","EXTRACTED"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_3").withString("extractionStatus","EXTRACTING"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_4").withString("extractionStatus","EXTRACTED"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_5").withString("extractionStatus","EXTRACTED"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_7").withString("extractionStatus","JOINING"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_8").withString("extractionStatus","SENT_TO_KAFKA"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_9").withString("extractionStatus","SENT_TO_KAFKA"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_6").withString("extractionStatus","SENT_TO_KAFKA")
		         );
		 dynamoDB.batchWriteItem(insertMultipleItems);
	 }
	 
	 public static void simulateBatch0CompleteBatch1Starting(AmazonDynamoDB dynamoDBClient) {
		 DynamoDB dynamoDB = new DynamoDB(dynamoDBClient);
		 TableWriteItems insertMultipleItems = new TableWriteItems(DPLTableName)
		     .withItemsToPut(
		    		 new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_1").withString("extractionStatus","AVAILABLE"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_2").withString("extractionStatus","EXTRACTED"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_3").withString("extractionStatus","EXTRACTING"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_4").withString("extractionStatus","EXTRACTED"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_5").withString("extractionStatus","EXTRACTED"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_7").withString("extractionStatus","READY_TO_JOIN"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_8").withString("extractionStatus","SENT_TO_KAFKA"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_9").withString("extractionStatus","SENT_TO_KAFKA"),
			         new Item().withPrimaryKey("sourceAvailDate",LocalDate.now().toString(),"tableName","SAMPLE_TABLE_6").withString("extractionStatus","SENT_TO_KAFKA")
		         );
		 dynamoDB.batchWriteItem(insertMultipleItems);
	 }

}
