package com.lcg.datafabric.customer.retail.service;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.dpl.extractor.dto.ExtractorSparkBatchReport;
import com.lcg.datafabric.utils.KafkaSender;

import ch.qos.logback.classic.Level;

@Component
public class ELKReportService {
	
	public final static String KEY_BOOTSTRAP_SERVER = "kafka.bootstrapServer";
	
	public static final String EXTRACTION_PER_VIEW_RECORDS = "EXTRACTION_PER_VIEW_RECORDS";
	public static final String RECORDS_SENT_TO_KAFKA = "RECORDS_SENT_TO_KAFKA";
	public static final String MC_JOIN_EVENT_MARKETING_RECORDS_BEFORE_TRANSFORM = "MC_JOIN_EVENT_MARKETING_RECORDS_BEFORE_TRANSFORM";
	
	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
	private KafkaSender kafkaSender;
	
	@Value("${kafka.topic.dplBatchReport}")
	private String targetTopic;
	
	@Autowired
	private DataFabricLogger2 logger;
	
	public ELKReportService(@Value("${kafka.bootstrapServer}") String kafkaBootstrapServer){
		
		Properties properties = new Properties();
		properties.put(KEY_BOOTSTRAP_SERVER, kafkaBootstrapServer);
		kafkaSender = KafkaSender.getInstance(properties);
	}

	public void sendExtractorInfoToELK( String serviceName, String batchNum, long recordsCount, String message) {
        try {

            ExtractorSparkBatchReport extractorSparkBatchReport = new ExtractorSparkBatchReport();
            extractorSparkBatchReport.setRecordCount(recordsCount);
            extractorSparkBatchReport.setBatchNo(batchNum);
            extractorSparkBatchReport.setServiceName(serviceName);
            extractorSparkBatchReport.setErrorMessage(message);

            byte[] messageBody = ObjectMapperHolder.getInstance().writeValueAsBytes(extractorSparkBatchReport);
            kafkaSender.sendMessage(targetTopic, batchNum, messageBody);
            
            logger.log(Level.INFO, ELKReportService.class, "sent extractor batch info to kafka. topic=" + targetTopic + " batchNum=" + batchNum +
                    " recordCount=" + recordsCount + " serviceName=" + serviceName, "INFO");

        } catch (JsonProcessingException jpe) {
        	logger.log(Level.WARN, ELKReportService.class, jpe.getMessage(), "WARN", jpe);
        }catch(Exception e) {
        	logger.log(Level.ERROR, ELKReportService.class, "Can't send log to elk because to exception", "ERROR", e);
        }
    }

    
    private String formatDateTime(Instant instant) {
        return instant.atZone(ZoneOffset.UTC).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
    
    private static class ObjectMapperHolder implements Serializable {

        private ObjectMapperHolder() {
        }

        public static ObjectMapper getInstance() {
            return Holder.instance;
        }

    }
    
    private static class Holder {

        private static transient ObjectMapper instance;

        static {
            instance = new ObjectMapper();
            instance.registerModule(new JavaTimeModule());
            instance.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        }

    }

}
